// In god we trust

#include <bits/stdc++.h>
#include <chrono>
#include <algorithm>
#include <signal.h>
#include <thread>

#define pb push_back
#define fi first
#define se second
#define Vi vector<int>
#define Vb vector<bool>
#define Pi pair<int, int>
#define MP make_pair
#define MT make_tuple
#define INF 1000000000
#define LINF 1000000000000000
#define int long long

using namespace std;
using namespace std::chrono;

int n, e;
vector<Vi> g;
int col[2020];
int ds[2020][200];
int ds_[2020][200];
int cur_obj = 0;

int idle = 0;
int idle_cycle = 0;
int col_cnt = 100;

Vi star;
int star_obj = -LINF;
vector<Vi> ds_star;
vector<Vi> ds__star;


int ti = 0;
int L = 300;
int TABU_MAX = 1000000;
int tabu[2020][200];


int rand(int min, int max){
  return rand()%(max-min + 1) + min;
}


int obj(int v, int c){
  return -ds[v][c] + ds[v][col[v]] - g[v].size() * (ds_[v][c] - ds_[v][col[v]]);
}


void perform_move(int v, int c){
  cur_obj += obj(v, c);

  for (int i = 0; i < g[v].size(); i++)
    ds[g[v][i]][col[v]] -= g[v].size(), ds[g[v][i]][c] += g[v].size(), ds_[g[v][i]][col[v]] --, ds_[g[v][i]][c] ++;

  col[v] = c;
}


void update_star(){
  if (cur_obj > star_obj){
    star_obj = cur_obj;
    star.resize(n);
    ds_star.resize(n);
    ds__star.resize(n);
    for (int i = 0; i < n; i++)
      ds_star[i] = Vi(), ds__star[i] = Vi();

    for (int i = 0; i < n; i++)
      star[i] = col[i];

    for (int i = 0; i < n; i++)
      for (int j = 0; j < col_cnt; j++)
	ds_star[i].pb(ds[i][j]), ds__star[i].pb(ds_[i][j]);

  }
}


void tabu_search(){
  int v = -1;
  int c = -1;
  int bst_obj = -LINF;
  for (int i = 0; i < n; i++)
    for (int j = 0; j < col_cnt; j++){
      int rel_obj = obj(i, j);
      if (rel_obj + cur_obj > star_obj or (tabu[i][j] <= ti and rel_obj > bst_obj))
	bst_obj = rel_obj, v = i, c = j;
    }
  if (v != -1)
    tabu[v][col[v]] = ti + L, perform_move(v, c);
  update_star();
  ti++;
}

void star_recover(){
  for (int i = 0; i < n; i++)
    col[i] = star[i];

  cur_obj = star_obj;

  for (int i = 0; i < n; i++)
    for (int j = 0; j < col_cnt; j++)
      ds[i][j] = ds_star[i][j], ds_[i][j] = ds__star[i][j];
}


signed main(int argc, char** argv){
  ios_base::sync_with_stdio(false);
  cin.tie(NULL);
  //signal(SIGINT, signal_callback_handler);
  using namespace std::this_thread;
  std::ifstream in(argv[1]);
  std::streambuf *cinbuf = std::cin.rdbuf(); //save old buf
  std::cin.rdbuf(in.rdbuf()); //redirect std::cin to in.txt!
  stringstream col_count(argv[2]);
  col_count >> col_cnt;
  stringstream tabu_tenure(argv[3]);
  tabu_tenure >> L;


  srand(time(NULL));

  cin >> n >> e;
  g.resize(n);

  int u, v;
  for (int i = 0; i < e; i++){
    cin >> u >> v;
    g[u].pb(v);
    g[v].pb(u);
  }



  for (int i = 0; i < n; i++){
    int c = rand(0, col_cnt - 1);
    col[i] = c;

    for (int j = 0; j < g[i].size(); j++)
      ds[g[i][j]][c] += g[i].size(), ds_[g[i][j]][c] ++;
  }

  for (int i = 0; i < n; i++)
    for (int j = 0; j < g[i].size(); j++)
      if (col[i] == col[g[i][j]])
	cur_obj += -g[i].size();


  for (idle_cycle = 0; idle_cycle < 10; idle_cycle++){
    idle = 0;
    ti = 0;
    for (int i = 0; i < n; i++)
      for (int j = 0; j < col_cnt; j++)
	tabu[i][j] = 0;

    for (idle = 0; idle < TABU_MAX; idle++){
      tabu_search();
      if (cur_obj == 0)
      break;
    }
    star_recover();
  }

  cout << col_cnt << " 0" << endl;
  for (int i = 0; i < n; i++)
    cout << col[i] << " ";
  cout << endl;


}




