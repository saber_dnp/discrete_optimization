#!/usr/bin/python
# -*- coding: utf-8 -*-
import os
from subprocess import Popen, PIPE

from constraint import *

lst = [6, 17, 16, 78, 16, 100]
h = 0


def make_not_equal_lambda():
    return lambda x, y: x != y


# a constraint programming finding a nice solution
def python_solve(input_data):
    global lst
    global h
    p = Problem()
    splitted_data = input_data.split()
    n, e = int(splitted_data[0]), int(splitted_data[1])
    for i in range(n):
        p.addVariable(i, list(range(lst[h])))

    for i in range(e):
        u, v = int(splitted_data[2 * i + 2]), int(splitted_data[2 * i + 3])
        p.addConstraint(make_not_equal_lambda(), (u, v))

    s = p.getSolution()
    ans = ""
    ans += str(n) + " 0\n"
    for i in range(n):
        ans += str(s[i]) + " "
    ans += '\n'
    h += 1
    return ans

# a tabu search method finding the competitive solution
def cpp_solve(input_data):
    global lst
    global h
    tmp_file_name = 'tmp.data'
    tmp_file = open(tmp_file_name, 'w')
    tmp_file.write(input_data)
    tmp_file.close()

    if lst[h] < 100:
        tabu_tenure = 300
    else:
        tabu_tenure = 1300

    process = Popen(['./a.out', tmp_file_name, str(lst[h]), str(tabu_tenure)], stdout=PIPE, universal_newlines=True)
    (stdout, stderr) = process.communicate()

    # removes the temporay file
    os.remove(tmp_file_name)
    h += 1
    return stdout.strip()


def solve_it(input_data):
    if 1:
        return cpp_solve(input_data)
    else:
        return python_solve(input_data)


if __name__ == '__main__':
    import sys

    if len(sys.argv) > 1:
        file_location = sys.argv[1].strip()
        with open(file_location, 'r') as input_data_file:
            input_data = input_data_file.read()
        print(solve_it(input_data))
    else:
        print(
            'This test requires an input file.  Please select one from the data directory. (i.e. python solver.py ./data/gc_4_1)')
