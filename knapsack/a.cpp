// In god we trust

// thanks to geeksforgeeks for branch and bound code

#include <bits/stdc++.h>
#include <chrono>
#include <algorithm>


#define pb push_back
#define fi first
#define se second
#define Vi vector<int>
#define Vb vector<bool>
#define Pi pair<int, int>
#define MP make_pair
#define MT make_tuple
#define INF 1000000000
#define int long long

using namespace std;
using namespace std::chrono;


// C++ program to solve knapsack problem using 
// branch and bound 
#include <bits/stdc++.h> 
using namespace std; 

// Structure for Item which store weight and corresponding 
// value of Item 
struct Item 
{ 
	float weight; 
	int value; 
}; 

// Node structure to store information of decision 
// tree 
struct Node 
{ 
	// level --> Level of node in decision tree (or index 
	//			 in arr[] 
	// profit --> Profit of nodes on path from root to this 
	//		 node (including this node) 
	// bound ---> Upper bound of maximum profit in subtree 
	//		 of this node/ 
	int level, profit, bound; 
	float weight;
  Vb opt;
}; 

// Comparison function to sort Item according to 
// val/weight ratio 
bool cmp(Item a, Item b) 
{ 
	double r1 = (double)a.value / a.weight; 
	double r2 = (double)b.value / b.weight; 
	return r1 > r2; 
} 

bool pair_cmp(pair<Item, int> a, pair<Item, int> b)
{
  double r1 = (double)a.fi.value / a.fi.weight;
  double r2 = (double)b.fi.value / b.fi.weight;
  return r1 > r2;
}

// Returns bound of profit in subtree rooted with u. 
// This function mainly uses Greedy solution to find 
// an upper bound on maximum profit. 
int bound(Node u, int n, int W, Item arr[]) 
{ 
	// if weight overcomes the knapsack capacity, return 
	// 0 as expected bound 
	if (u.weight >= W) 
		return 0; 

	// initialize bound on profit by current profit 
	int profit_bound = u.profit; 

	// start including items from index 1 more to current 
	// item index 
	int j = u.level + 1; 
	int totweight = u.weight; 

	// checking index condition and knapsack capacity 
	// condition 
	while ((j < n) && (totweight + arr[j].weight <= W)) 
	{ 
		totweight += arr[j].weight; 
		profit_bound += arr[j].value; 
		j++; 
	} 

	// If k is not n, include last item partially for 
	// upper bound on profit 
	if (j < n) 
		profit_bound += (W - totweight) * arr[j].value / 
										arr[j].weight; 

	return profit_bound; 
} 

// Returns maximum profit we can get with capacity W 
bool knapsack(int W, Item arr[], int n)
{

    int time_limit = 10;
    auto start = high_resolution_clock::now();

	// sorting Item on basis of value per unit 
	// weight.

  int ref[n];
  pair<Item, int> temp[n];
  for (int i = 0; i < n; i++)
    temp[i] = MP(arr[i], i);
  sort(temp, temp + n, pair_cmp);
  for (int i = 0 ; i < n; i++)
    ref[i] = temp[i].se;
  
	sort(arr, arr + n, cmp); 

	// make a queue for traversing the node 
	queue<Node> Q; 
	Node u, v; 

	// dummy node at starting 
	u.level = -1; 
	u.profit = u.weight = 0; 
	Q.push(u); 

	// One by one extract an item from decision tree 
	// compute profit of all children of extracted item 
	// and keep saving maxProfit 
	int maxProfit = 0;
	Vb best_opt;
	bool time_out = false;
	while (!Q.empty()) 
	{
	    auto now_ = high_resolution_clock::now();
	    auto duration = duration_cast<seconds>(now_ - start);
	    if (duration.count() > time_limit){
	        return false;
	    }
		// Dequeue a node 
		u = Q.front(); 
		Q.pop(); 

		// If it is starting node, assign level 0 
		if (u.level == -1) 
			v.level = 0; 

		// If there is nothing on next level 
		if (u.level == n-1) 
			continue; 

		// Else if not last node, then increment level, 
		// and compute profit of children nodes.
		v.opt = u.opt;
		v.level = u.level + 1; 

		// Taking current level's item add current 
		// level's weight and value to node u's 
		// weight and value 
		v.weight = u.weight + arr[v.level].weight; 
		v.profit = u.profit + arr[v.level].value; 
		v.opt.pb(1);
		// If cumulated weight is less than W and 
		// profit is greater than previous profit, 
		// update maxprofit 
		if (v.weight <= W && v.profit > maxProfit) 
		  maxProfit = v.profit, best_opt = v.opt; 

		// Get the upper bound on profit to decide 
		// whether to add v to Q or not. 
		v.bound = bound(v, n, W, arr); 

		// If bound value is greater than profit, 
		// then only push into queue for further 
		// consideration 
		if (v.bound > maxProfit) 
			Q.push(v); 

		// Do the same thing, but Without taking 
		// the item in knapsack 
		v.weight = u.weight; 
		v.profit = u.profit;
		v.opt[v.opt.size() - 1] = 0;
		v.bound = bound(v, n, W, arr); 
		if (v.bound > maxProfit) 
			Q.push(v); 
	} 


    bool ans[n];
	cout << maxProfit << " 1" << endl;
	for (int i = 0; i < n; i++){
	  if (i < best_opt.size() and best_opt[i])
	    ans[ref[i]] = 1;
	  else
	    ans[ref[i]] = 0;
	}
	for (int i = 0; i < n; i++)
	  cout << ans[i] << " ";
	cout << endl;
    return true;
} 

// driver program to test above function 
signed main(int argc, char** argv)
{
    std::ifstream in(argv[1]);
    std::streambuf *cinbuf = std::cin.rdbuf(); //save old buf
    std::cin.rdbuf(in.rdbuf()); //redirect std::cin to in.txt!
  int n, k;
  cin >> n >> k;

  int weights[n], values[n];
  
  for (int i = 0 ; i < n; i++)
    cin >> values[i] >> weights[i];
  Item arr[n];
  for (int i = 0; i < n; i++)
    arr[i] = {weights[i], values[i]};
  if (not knapsack(k, arr, n)){
  int capacity = k;
  int items = n;
  static int dp[100100][220];
  static int t [100100][220];

  for (int j = 0; j <= items; j++) {
    dp[0][j] = 0;
    t[0][j] = 0;
  }
  for (int i = 0; i <= capacity; i++) {
    dp[i][0] = 0;
    t[i][0] = 0;
  }

  for (int i = 1; i <= capacity; i++)
    for (int j = 1; j <= items; j++){
      if (weights[j - 1] <= i && values[j - 1] + dp[i - weights[j - 1]][j - 1] > dp[i][j - 1]) {
	dp[i][j] = values[j - 1] + dp[i - weights[j - 1]][j - 1];
	t[i][j] = i - weights[j - 1];
      }
      else {
	dp[i][j] = dp[i][j - 1];
	t[i][j] = i;
      }
    }

  cout << dp[capacity][items] << " 1" << endl;
  Vi v;
  int i = capacity;
  for (int j = items; j >= 1; j--) {
    if (t[i][j] == i)
      v.pb(0);
    else {
      v.pb(1);
      i = t[i][j];
    }
  }

  for (int i = n - 1; i >= 0; i--)
    cout << v[i] << " ";
  cout << endl;
  }
	return 0; 
} 


