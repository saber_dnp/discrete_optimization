This is a hybrid method for the knapsack problem combining dp and branch and bound.
 
Actually it tries to tackle the problem by branch and bound method within a time limit. If it couldn't make to the end in the interval it defaults to the dynamic programming method.

Both methods' implementation is very basic and you could understand easily. So good luck.

p.s. This implementation is in a.cpp. In any case running into segmentation fault there should be of the dp array dimensions. So don't panic in case, This is the future work!!


Warning: You have to compile the a.cpp file every time you make a change to that. I am using a.out file to run the code from python.